#!/bin/bash

docker --login dppdatalab
docker build -f docker/Dockerfile -t dppdatalab/elasticsearch
docker push dppdatalab/elasticsearch
