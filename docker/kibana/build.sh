#!/bin/bash

docker --login dppdatalab
docker build -f docker/Dockerfile -t dppdatalab/kibana
docker push dppdatalab/kibana
